package eu.themaxik.queue.util;

import eu.themaxik.queue.main.QueuePlugin;


import java.util.concurrent.TimeUnit;

public class Ticker {

    private QueuePlugin queuePlugin;
    private SlotChecker slotChecker;
    private TabHeader tabHeader;

    public Ticker(QueuePlugin queuePlugin, SlotChecker slotChecker, TabHeader tabHeader) {
        this.queuePlugin = queuePlugin;
        this.slotChecker = slotChecker;
        this.tabHeader = tabHeader;
    }


    public void startTicking() {
        queuePlugin.getProxy().getScheduler().schedule(queuePlugin, () -> {
            tabHeader.sendTabHeaderToQue();
            slotChecker.checkAndTrigger();
        }, 0, 20, TimeUnit.SECONDS);
    }

}
