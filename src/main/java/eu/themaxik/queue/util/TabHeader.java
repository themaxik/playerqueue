package eu.themaxik.queue.util;

import eu.themaxik.queue.main.QueuePlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.packet.Chat;

public class TabHeader {

    private QueuePlugin queuePlugin;
    private QueueManager queueManager;
    private String shopLink;

    public TabHeader(QueuePlugin queuePlugin, QueueManager queueManager) {
        this.queuePlugin = queuePlugin;
        this.queueManager = queueManager;
        shopLink = queuePlugin.getConfig().getString("ShopLink");
    }

    public void sendTabHeaderToQue() {
        for (ProxiedPlayer pp : queueManager.getPriority()) {
            if (queueManager.isInQueue(pp)) {
                sendTabToPlayer(pp, timeToString(getWaitSeconds(pp)));
            }
        }
        for (ProxiedPlayer pp : queueManager.getWaiting()) {
            if (queueManager.isInQueue(pp)) {
                sendTabToPlayer(pp, timeToString(getWaitSeconds(pp)));
            }
        }
    }

    private void sendTabToPlayer(ProxiedPlayer proxiedPlayer, String waitTime) {
        TextComponent tabHeader = new TextComponent("                ");
        tabHeader.addExtra(ChatColor.GREEN + "Position in Queue: " + ChatColor.GOLD + queueManager.getShownPosition(proxiedPlayer));
        tabHeader.addExtra("                ");
        tabHeader.addExtra(ChatColor.GREEN + "\nEstimated queue time: " + ChatColor.RED + ChatColor.BOLD + waitTime);

        TextComponent tabFooter = new TextComponent(ChatColor.GREEN + "Get Priority Queue at:");
        tabFooter.addExtra("\n" + ChatColor.GOLD + ChatColor.UNDERLINE + shopLink);

        proxiedPlayer.setTabHeader(tabHeader, tabFooter);
    }

    private String timeToString(int seconds) {
        seconds = seconds + 120; //120 ist just a buffer so it doesnt show 0m at pos 13
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        return hours + "h:" + minutes + "m";
    }

    private int getWaitSeconds(ProxiedPlayer pp) {
        float multiplier = queuePlugin.getConfig().getFloat("WaitTimeMultiplier");
        return (int) ((queueManager.getShownPosition(pp) + 120) * (multiplier + 1));
    }
}
