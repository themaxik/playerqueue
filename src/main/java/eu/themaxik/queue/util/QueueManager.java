package eu.themaxik.queue.util;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import java.util.concurrent.CopyOnWriteArrayList;

public class QueueManager {

    @Getter
    private CopyOnWriteArrayList<ProxiedPlayer> priority = new CopyOnWriteArrayList<>();
    @Getter
    private CopyOnWriteArrayList<ProxiedPlayer> waiting = new CopyOnWriteArrayList<>();

    public void queueIn(ProxiedPlayer proxiedPlayer) {
        if (proxiedPlayer.hasPermission("queue.priority")) {
            addToPriorityQue(proxiedPlayer);
        } else {
            addToNormalQue(proxiedPlayer);
        }
    }

    /**
     * Returns the position in Queue to get displayed to the user
     * Basically PrioQue.size + PoisitonInHisQueue
     */
    public int getShownPosition(ProxiedPlayer proxiedPlayer) {
        int pos = getPosition(proxiedPlayer) + 1;
        if (!priority.contains(proxiedPlayer)) {
            pos = pos + priority.size();
        }
        return pos;
    }

    public ProxiedPlayer getNextPlayer(){
        clearEmpty();
        if(!priority.isEmpty()){
           return priority.get(0);
        }else if (!waiting.isEmpty()){
            return waiting.get(0);
        }
        return null;
    }

    public void removeFromQueue(ProxiedPlayer proxiedPlayer){
        boolean removed = false;
        if(priority.contains(proxiedPlayer)){
            priority.remove(proxiedPlayer);
            removed = true;
        }
        if(waiting.contains(proxiedPlayer)){
            waiting.remove(proxiedPlayer);
            removed = true;
        }
        //when something changed
        if(removed) informQueuePosition();
    }

    public void clearEmpty(){
        for(ProxiedPlayer pp : waiting){
            if(pp == null || !pp.isConnected()){
                removeFromQueue(pp);
            }
        }
        for(ProxiedPlayer pp : priority){
            if(pp == null || !pp.isConnected()){
                removeFromQueue(pp);
            }
        }
    }

    public void informQueuePosition(){
        for(ProxiedPlayer pp : waiting)
            pp.sendMessage(ChatColor.GREEN + "Position in Queue: " + ChatColor.YELLOW + getShownPosition(pp));
        for(ProxiedPlayer pp : priority){
            pp.sendMessage(ChatColor.GREEN + "Position in Queue: " + ChatColor.YELLOW + getShownPosition(pp));
        }
    }

    public int getQueSize(){
        return waiting.size() + priority.size();
    }

    public boolean isInQueue(ProxiedPlayer proxiedPlayer){
        if(waiting.contains(proxiedPlayer) || priority.contains(proxiedPlayer)){
            return true;
        }
        return false;
    }

    private void addToPriorityQue(ProxiedPlayer proxiedPlayer) {
        if (waiting.contains(proxiedPlayer)) {
            waiting.remove(proxiedPlayer);
        }
        if (!priority.contains(proxiedPlayer)) {
            priority.add(proxiedPlayer);
            proxiedPlayer.sendMessage(ChatColor.GREEN + "You are now in " + ChatColor.GOLD + "Priority" + ChatColor.GREEN + " Queue");
            proxiedPlayer.sendMessage(ChatColor.GREEN + "Position in Queue: " + ChatColor.YELLOW + getShownPosition(proxiedPlayer));
        }
    }

    private void addToNormalQue(ProxiedPlayer proxiedPlayer) {
        if (priority.contains(proxiedPlayer)) {
            priority.remove(proxiedPlayer);
        }
        if (!waiting.contains(proxiedPlayer)) {
            waiting.add(proxiedPlayer);
            proxiedPlayer.sendMessage(ChatColor.GREEN + "You are now in " + ChatColor.GRAY + "Basic" + ChatColor.GREEN + " Queue");
            proxiedPlayer.sendMessage(ChatColor.GREEN + "Position in Queue: " +
                    ChatColor.YELLOW + getShownPosition(proxiedPlayer));
        }
    }

    private int getPosition(ProxiedPlayer proxiedPlayer) {
        if (waiting.contains(proxiedPlayer)) {
            for (int i = 0; waiting.size() > i; i++) {
                if (waiting.get(i).equals(proxiedPlayer)) {
                    return i;
                }
            }
        } else if (priority.contains(proxiedPlayer)) {
            for (int i = 0; priority.size() > i; i++) {
                if (priority.get(i).equals(proxiedPlayer)) {
                    return i;
                }
            }
        }
        return 0;
    }
}
