package eu.themaxik.queue.util;

import eu.themaxik.queue.main.QueuePlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class SlotChecker {

    private QueuePlugin queuePlugin;
    private QueueManager queueManager;

    public SlotChecker(QueuePlugin queuePlugin, QueueManager queueManager) {
        this.queuePlugin = queuePlugin;
        this.queueManager = queueManager;
    }

    public void checkAndTrigger() {
        int freeSlots = queuePlugin.getConfig().getInt("MaxPlayers") -
                queuePlugin.getProxy().getServerInfo(queuePlugin.getConfig().getString("TargetServer")).getPlayers().size();


        if (hasFreeSlots()) {
            allowJoin();
            if(freeSlots < 40){
              return;
            }
            for (int i = 0; 30 >= i; i++) {
                allowJoin();
            }
        }
    }

    private void allowJoin() {
        if(!hasFreeSlots()) return;

        ProxiedPlayer proxiedPlayer = queueManager.getNextPlayer();
        if (proxiedPlayer == null) {
            return;
        }
        String TargetServer = queuePlugin.getConfig().getString("TargetServer");
        proxiedPlayer.sendMessage(ChatColor.AQUA + "Sending you to Server: " + ChatColor.GOLD + TargetServer);

        proxiedPlayer.connect(queuePlugin.getProxy().getServerInfo(TargetServer), (b,r) ->{
            if(b){
                queueManager.removeFromQueue(proxiedPlayer);
            }else{
                proxiedPlayer.sendMessage(ChatColor.RED + "Whoops! That Did not work.");
            }
        });

    }

    private boolean hasFreeSlots(){
        int freeSlots = queuePlugin.getConfig().getInt("MaxPlayers") -
                queuePlugin.getProxy().getServerInfo(queuePlugin.getConfig().getString("TargetServer")).getPlayers().size();
        if(freeSlots > 0){
            return true;
        }else return false;
    }



}
