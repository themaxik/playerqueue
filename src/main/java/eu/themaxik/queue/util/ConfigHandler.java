package eu.themaxik.queue.util;

import eu.themaxik.queue.main.QueuePlugin;
import lombok.Getter;
import lombok.SneakyThrows;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogRecord;

@Getter
public class ConfigHandler {

    private final File confFile;
    private final QueuePlugin queuePlugin;
    private Configuration config;


    public ConfigHandler(QueuePlugin queuePlugin) {
        this.queuePlugin = queuePlugin;
        this.confFile = new File(getQueuePlugin().getDataFolder().getPath(), "\\config.yml");

        createConfig();
    }

    @SneakyThrows
    private void createConfig() {
        if (!queuePlugin.getDataFolder().exists()) {
            queuePlugin.getDataFolder().mkdir();
        }
        if (confFile.exists()) {
            loadConfig();
            return;
        }
        confFile.createNewFile();
        loadConfig();
    }

    private void loadConfig() {
        try {
            this.config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(confFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (config == null) {
            queuePlugin.getLogger().log(new LogRecord(Level.WARNING, "Cannot load Config"));
        }
    }

    public void saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, confFile);
        } catch (IOException e) {
            queuePlugin.getLogger().log(new LogRecord(Level.WARNING, "Error saving Config for Queue"));
            e.printStackTrace();
        }
    }


}
