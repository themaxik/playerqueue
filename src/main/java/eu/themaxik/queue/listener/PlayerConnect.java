package eu.themaxik.queue.listener;

import eu.themaxik.queue.main.QueuePlugin;
import eu.themaxik.queue.util.QueueManager;
import eu.themaxik.queue.util.SlotChecker;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerConnect implements Listener {

    private QueuePlugin queuePlugin;
    private QueueManager queueManager;
    private SlotChecker slotChecker;

    public PlayerConnect(QueuePlugin queuePlugin, QueueManager queueManager, SlotChecker slotChecker) {
        this.queuePlugin = queuePlugin;
        this.queueManager = queueManager;
        this.slotChecker = slotChecker;
    }


    @EventHandler
    public void onPlayerConnect(PostLoginEvent event) {
        queueManager.queueIn(event.getPlayer());
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent event) {
        queueManager.removeFromQueue(event.getPlayer());
        queueManager.clearEmpty();
    }

    @EventHandler
    public void onPlayerSwitchServer(ServerSwitchEvent event) {
        if (!event.getPlayer().getServer().getInfo().getName().equals(queuePlugin.getConfig().getString("TargetServer"))) {
            queueManager.queueIn(event.getPlayer());
            return;
        }
        queueManager.removeFromQueue(event.getPlayer());
    }

}
