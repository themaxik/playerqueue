package eu.themaxik.queue.main;

import eu.themaxik.queue.commands.PositionCommand;
import eu.themaxik.queue.listener.PlayerConnect;
import eu.themaxik.queue.util.*;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.config.Configuration;

public class QueuePlugin extends Plugin {

    private QueueManager queueManager;
    private SlotChecker slotChecker;
    private Ticker ticker;
    @Getter
    private Configuration config;
    private ConfigHandler ConfigHandler;
    private TabHeader tabHeader;

    @Override
    public void onEnable() {
        ConfigHandler = new ConfigHandler(this);
        config = ConfigHandler.getConfig();

        queueManager = new QueueManager();
        slotChecker = new SlotChecker(this, queueManager);
        tabHeader = new TabHeader(this, queueManager);
        ticker = new Ticker(this, slotChecker, tabHeader);

        ticker.startTicking();
        registerEvents();
        getProxy().getPluginManager().registerCommand(this, new PositionCommand(queueManager));
    }

    private void registerEvents() {
        PluginManager pluginManager = getProxy().getPluginManager();
        pluginManager.registerListener(this, new PlayerConnect(this, queueManager, slotChecker));
    }

    public void onDisable() {
        ConfigHandler.saveConfig();
    }

}
