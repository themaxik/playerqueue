package eu.themaxik.queue.commands;

import eu.themaxik.queue.util.QueueManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class PositionCommand extends Command {

    private QueueManager queueManager;

    public PositionCommand(QueueManager queueManager) {
        super("position");
        this.queueManager = queueManager;
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        proxiedPlayer.sendMessage(queueManager.getShownPosition(proxiedPlayer) + "");
    }
}
